# README #

### Setting Up Environment ###

* pip install flask
* Setup MySQL if not already installed
* Change MySQL credentials in app.py

### Notes About This App ###

* Database setup code is in the sql folder
* Layout.html contains the template code for the rest of the HTML files in the templates folder
* CSS file is in the static directory
* app.py contatins all of the functionality
* app uses AJAX to asynchronously communicate with the database (AJAX code is found in login.html and signup.html)

### Things I Wanted to Add to this App ###

* Wanted to use email confirmations for signup as well as add a forgot password password feature using tokens, but did not have a mail server to connect to