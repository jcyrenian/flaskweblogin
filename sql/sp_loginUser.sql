DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_loginUser`(
    IN p_username VARCHAR(45)
)
BEGIN
    IF ( select exists (select 1 from tbl_user where user_username = p_username) ) THEN
     
        select user_password from tbl_user where user_username = p_username;
     
    ELSE
     
        select 'Fail';
     
    END IF;
END$$
DELIMITER ;