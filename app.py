from flask import Flask, render_template, json, request
from flask.ext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash

app = Flask(__name__)
mysql = MySQL()
 
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'My5ql@dm1n'
app.config['MYSQL_DATABASE_DB'] = 'WebLogin'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

conn = mysql.connect()
cursor = conn.cursor()


@app.route("/")
def main():
    return render_template('index.html')

@app.route('/signup')
def signup():
    return render_template('signup.html')
    
@app.route('/login')
def login():
    return render_template('login.html')

# create new user
@app.route('/createUser',methods=['POST'])
def signUp():
    # read the posted values from the UI
    _name = request.form['inputName']
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']
    
    if _name and _email and _password:
        _hashed_password = generate_password_hash(_password)
        cursor.callproc('sp_createUser',(_name,_email,_hashed_password))

        data = cursor.fetchall()
    
        # validate the received values
        if len(data) is 0:
            conn.commit()
            return json.dumps({'message':'User created successfully !'})
        else:
            return json.dumps({'error':str(data[0])})
    else:
        return json.dumps({'validationError':'Fill in all fields'})

# sign in
@app.route('/loginUser',methods=['POST'])
def logIn():
    # read the posted values from the UI
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']
    
    cursor.callproc('sp_loginUser',[_email,])

    data = cursor.fetchone()[0]
    
    # validate the received values
    if check_password_hash(data, _password):
        conn.commit()
        return json.dumps({'success':'Login Worked!'})
    else:
        return json.dumps({'fail':'Login Failed!'})
    
if __name__ == "__main__":
    app.run()